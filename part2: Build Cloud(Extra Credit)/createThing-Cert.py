################################################### Connecting to AWS
import os
import boto3

import json
################################################### Create random name for things
import random
import string

################################################### Parameters for Thing
thingArn = ''
thingId = ''
defaultPolicyName = 'lab4-policy'
###################################################

def createThing(id, thingName):
  global thingClient
  thingResponse = thingClient.create_thing(
      thingName = thingName
  )
  print("created device{}".format(id))
  data = json.loads(json.dumps(thingResponse, sort_keys=False, indent=4))
  for element in data: 
    if element == 'thingArn':
        thingArn = data['thingArn']
    elif element == 'thingId':
        thingId = data['thingId']
    createCertificate(id, thingName)

def createCertificate(id, thingName):
    global thingClient
    certResponse = thingClient.create_keys_and_certificate(
            setAsActive = True
    )
    data = json.loads(json.dumps(certResponse, sort_keys=False, indent=4))
    for element in data: 
            if element == 'certificateArn':
                    certificateArn = data['certificateArn']
            elif element == 'keyPair':
                    PublicKey = data['keyPair']['PublicKey']
                    PrivateKey = data['keyPair']['PrivateKey']
            elif element == 'certificatePem':
                    certificatePem = data['certificatePem']
            elif element == 'certificateId':
                    certificateId = data['certificateId']
    for f, content in [("./certificates/device_{}/device_{}.public.pem".format(id,id),PublicKey),("./certificates/device_{}/device_{}.private.pem".format(id,id),PrivateKey),("./certificates/device_{}/device_{}.certificate.pem".format(id,id),certificatePem)]:
        os.makedirs(os.path.dirname(f), exist_ok=True)					
        with open(f, 'w') as outfile:
            outfile.write(content)                       

    response = thingClient.attach_policy(
            policyName = defaultPolicyName,
            target = certificateArn
    )
    print("attached policy to device{}".format(id))

    response = thingClient.attach_thing_principal(
            thingName = thingName,
            principal = certificateArn
    )
    print("attached certificate to device{}".format(id))



thingClient = boto3.client('iot', aws_access_key_id='AKIAXAMM5OAODICT6OS4', aws_secret_access_key='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', region_name='us-east-1')

if __name__ == "__main__":
        for t in range(10):
                thingName = 'lab4group-'+''.join([random.choice(string.ascii_letters + string.digits) for n in range(15)])
                createThing(t, thingName)
